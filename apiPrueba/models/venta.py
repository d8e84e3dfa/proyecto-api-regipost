from django.db                   import models
from django.db                   import models
from django.contrib.auth.models  import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.contrib.auth.hashers import make_password

class Ventas(models.Model):
    codigo_ventas    = models.IntegerField(db_column='CODIGO_VENTAS', primary_key= True)  # Field name made lowercase.
    codigo_cliente   = models.IntegerField(db_column='CODIGO_CLIENTE')  # Field name made lowercase.
    codigo_usuario   = models.IntegerField(db_column='CODIGO_USUARIO')  # Field name made lowercase.
    tipo_documento   = models.CharField(db_column='TIPO_DOCUMENTO', max_length=50)  # Field name made lowercase.
    serie            = models.IntegerField(db_column='SERIE')  # Field name made lowercase.
    numeracion       = models.IntegerField(db_column='NUMERACION')  # Field name made lowercase.
    tipo_pago        = models.CharField(db_column='TIPO_PAGO', max_length=50)  # Field name made lowercase.
    fecha_venta      = models.DateTimeField(db_column='FECHA_VENTA', blank=True, null=True)  # Field name made lowercase.
    monto_recibido   = models.IntegerField(db_column='MONTO_RECIBIDO', blank=True, null=True)  # Field name made lowercase.
    total            = models.IntegerField(db_column='TOTAL')  # Field name made lowercase.
    total_tarjeta    = models.IntegerField(db_column='TOTAL_TARJETA')  # Field name made lowercase.
    codigo_bodega    = models.IntegerField(db_column='CODIGO_BODEGA')  # Field name made lowercase.
    descripcion      = models.TextField(db_column='DESCRIPCION')  # Field name made lowercase.
    estado           = models.CharField(db_column='ESTADO', max_length=15)  # Field name made lowercase.
    descuento        = models.IntegerField(db_column='DESCUENTO')  # Field name made lowercase.
    codigo_vendedor  = models.IntegerField(db_column='CODIGO_VENDEDOR')  # Field name made lowercase.
    
    class Meta:
        managed = False
        db_table = 'ventas'
    