
from django.db                   import models
from django.contrib.auth.models  import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.contrib.auth.hashers import make_password

class DetalleVentas(models.Model):
    codigo_detalle_ventas = models.IntegerField(db_column='CODIGO_DETALLE_VENTAS')  # Field name made lowercase.
    codigo_ventas = models.IntegerField(db_column='CODIGO_VENTAS',primary_key= True)  # Field name made lowercase.
    codigo_producto = models.IntegerField(db_column='CODIGO_PRODUCTO')  # Field name made lowercase.
    cantidad = models.IntegerField(db_column='CANTIDAD')  # Field name made lowercase.
    precio = models.IntegerField(db_column='PRECIO')  # Field name made lowercase.
    igv = models.IntegerField(db_column='IGV')  # Field name made lowercase.
    subtotal = models.IntegerField(db_column='SUBTOTAL')  # Field name made lowercase.
    estado = models.CharField(db_column='ESTADO', max_length=15)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'detalle_ventas'