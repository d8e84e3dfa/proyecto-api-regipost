from django.conf import settings
from django.http import request
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from apiPrueba.models.detalle_venta import DetalleVentas
from apiPrueba.serializers.detalle_ventaSerializer import DetalleVentasSerializer
from rest_framework_simplejwt.backends import TokenBackend


class DetalleView(generics.ListAPIView):
    serializer_class = DetalleVentasSerializer
    #filterset_fields = ['codigo_ventas']  
    permission_classes = (IsAuthenticated,)
    def get_queryset(self, *args, ):
        codigo = self.kwargs["pk"]
                                   
        return DetalleVentas.objects.filter(codigo_ventas=codigo)
        
    def get(self, request, *args, **kwargs):
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)       
        return super().get(request, *args, **kwargs)

  
   
    
    
    