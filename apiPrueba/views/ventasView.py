from django.conf import settings
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from apiPrueba.models.venta import Ventas
from apiPrueba.serializers.ventaSerializer import VentasSerializer
from rest_framework_simplejwt.backends import TokenBackend


class VentasDetailView(generics.RetrieveAPIView):
    queryset = Ventas.objects.all()
    serializer_class = VentasSerializer
    permission_classes = (IsAuthenticated,)
    
       

    def get(self, request, *args, **kwargs):
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)       
        return super().get(request, *args, **kwargs)