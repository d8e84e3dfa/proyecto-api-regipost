from apiPrueba.models.venta                   import Ventas
from apiPrueba.models.detalle_venta                import DetalleVentas
from rest_framework                               import serializers
from apiPrueba.serializers.detalle_ventaSerializer import DetalleVentasSerializer


class VentasSerializer(serializers.ModelSerializer):
    detalle=DetalleVentasSerializer()
    
    
    class Meta:
        model  = Ventas
        
        fields = ['codigo_ventas', 'codigo_cliente', 'codigo_usuario', 'tipo_documento', 'serie', 'numeracion', 'tipo_pago', 'fecha_venta', 'monto_recibido', 'total', 'total_tarjeta', 'codigo_bodega', 'descripcion', 'estado', 'descuento', 'codigo_vendedor']
        
        
    def to_representation(self, obj):
        venta   = Ventas.objects.get(codigo_ventas=obj.codigo_ventas)
        precios=[]
        codigo_producto=[]
        cantidad=[]
        
        
        detalle = DetalleVentas.objects.filter(codigo_ventas=obj.codigo_ventas)
        for ventas in detalle:
            precios.append(ventas.precio)
            codigo_producto.append(ventas.codigo_producto)
            cantidad.append(ventas.cantidad)


        return {
            
    "Token": "serial",
    "User": "idCaja",
    "Empresa": "numZ[0]",
    "Facturas": [
        {       
            "Ventas" : {
                    "IdFactura":  venta.numeracion,
                    "CodSucursal": "",
                    "CodClaseFactura": "",
                    "CodCliente": str(venta.codigo_cliente),
                    "CodCondicionPago": "",
                    "FechaFactura": venta.fecha_venta,
                    "FechaVencimiento": venta.fecha_venta,
                    "Observaciones": "",
                    "Contado": venta.total,
                    "NoOrdenCompra": "",
                    "NitLibranzas": "",
                    "CodVendedor": venta.codigo_usuario,
                    "PorcAdministracion": 0.0,
                    "PorcInprevistos": 0.0,
                    "PorcUtilidad": 0.0,
                    "NoContrato": "",
                    "CodMoneda": "PESOS",
                    "TasaCambio": 1.0,
                    },
           "detalle": [
                {
                    "IdFactura": venta.numeracion,
                    "CodSucursal": "",
                    "CodProducto": codigo_producto,
                    "NombreProducto": "",
                    "DescripcionAdicional": "",
                    "CodUnidad": "",
                    "Cantidad": cantidad,
                    "CodListaPrecios": "",
                    "PrecioVenta": precios,
                    "CodImpuesto": "",
                    "Serial": "",
                    "CodLote": "",
                    "CodTalla": "",
                    "CodColor": "",
                    "CodVendedor": ""
                }
            ],
            "pago": [
                {
                    "IdFactura": venta.numeracion,
                    "CodSucursal": "",
                    "CodFormaPago": "",
                    "ValorFormaPago": 0.0,
                    "NoDocumento": "",
                    "NombreDocumento": "",
                    "CodBanco": ""
                }
            ],
            "retenciones": [
                {
                    "IdFactura": venta.numeracion,
                    "CodSucursal": "",
                    "CodImpuesto": ""
                }
            ]
        }
    ]
}       
        