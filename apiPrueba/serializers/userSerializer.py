from apiPrueba.models.user                   import User
from rest_framework                               import serializers


class UserSerializer(serializers.ModelSerializer):
    
    class Meta:
        model  = User
        fields = ['id', 'username', 'password']

    def create(self, validated_data):
        userInstance = User.objects.create(**validated_data)
        return userInstance

    