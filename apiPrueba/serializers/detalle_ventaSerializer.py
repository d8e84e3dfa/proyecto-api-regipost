from apiPrueba.models.detalle_venta import DetalleVentas
from rest_framework import serializers

class DetalleVentasSerializer(serializers.ModelSerializer):
    class Meta:
        model = DetalleVentas
        fields = '__all__'
        managed = False


        