# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Bodegas(models.Model):
    codigo_bodega = models.AutoField(db_column='CODIGO_BODEGA', primary_key=True)  # Field name made lowercase.
    nombre = models.CharField(db_column='NOMBRE', max_length=50)  # Field name made lowercase.
    fecha_creacion = models.DateTimeField(db_column='FECHA_CREACION')  # Field name made lowercase.
    estado = models.CharField(db_column='ESTADO', max_length=10)  # Field name made lowercase.
    codigo_bodega_venta = models.IntegerField(db_column='CODIGO_BODEGA_VENTA')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'bodegas'


class Categoria(models.Model):
    codigo_categoria = models.AutoField(db_column='CODIGO_CATEGORIA', primary_key=True)  # Field name made lowercase.
    descripcion = models.CharField(db_column='DESCRIPCION', max_length=100)  # Field name made lowercase.
    estado = models.CharField(db_column='ESTADO', max_length=10)  # Field name made lowercase.
    actualizado = models.CharField(db_column='ACTUALIZADO', max_length=5, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'categoria'


class Cliente(models.Model):
    codigo_cliente = models.AutoField(db_column='CODIGO_CLIENTE', primary_key=True)  # Field name made lowercase.
    tipo_contribuyente = models.CharField(db_column='TIPO_CONTRIBUYENTE', max_length=30)  # Field name made lowercase.
    tipo_documento = models.CharField(db_column='TIPO_DOCUMENTO', max_length=5)  # Field name made lowercase.
    documento = models.CharField(db_column='DOCUMENTO', max_length=20)  # Field name made lowercase.
    nombre_cliente = models.CharField(db_column='NOMBRE_CLIENTE', max_length=100)  # Field name made lowercase.
    direccion = models.CharField(db_column='DIRECCION', max_length=100)  # Field name made lowercase.
    telefono = models.CharField(db_column='TELEFONO', max_length=12, blank=True, null=True)  # Field name made lowercase.
    email = models.CharField(db_column='EMAIL', max_length=100, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'cliente'


class Compras(models.Model):
    codigo_compras = models.AutoField(db_column='CODIGO_COMPRAS', primary_key=True)  # Field name made lowercase.
    codigo_proveedor = models.IntegerField(db_column='CODIGO_PROVEEDOR')  # Field name made lowercase.
    tipo_documento = models.CharField(db_column='TIPO_DOCUMENTO', max_length=30)  # Field name made lowercase.
    serie = models.CharField(db_column='SERIE', max_length=4)  # Field name made lowercase.
    nro_comprobante = models.CharField(db_column='NRO_COMPROBANTE', max_length=7)  # Field name made lowercase.
    tipo_pago = models.CharField(db_column='TIPO_PAGO', max_length=20)  # Field name made lowercase.
    fecha_compra = models.DateField(db_column='FECHA_COMPRA')  # Field name made lowercase.
    total = models.IntegerField(db_column='TOTAL')  # Field name made lowercase.
    estado = models.CharField(db_column='ESTADO', max_length=15)  # Field name made lowercase.
    codigo_bodega = models.IntegerField(db_column='CODIGO_BODEGA')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'compras'


class Configuracion(models.Model):
    codigo_configuracion = models.AutoField(db_column='CODIGO_CONFIGURACION', primary_key=True)  # Field name made lowercase.
    nombre = models.CharField(db_column='NOMBRE', max_length=100)  # Field name made lowercase.
    valor = models.CharField(db_column='VALOR', max_length=100, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'configuracion'


class ConfiguracionRef(models.Model):
    codigo_configuracion_ref = models.AutoField(db_column='CODIGO_CONFIGURACION_REF', primary_key=True)  # Field name made lowercase.
    nombre = models.CharField(db_column='NOMBRE', max_length=100)  # Field name made lowercase.
    valor = models.CharField(db_column='VALOR', max_length=3)  # Field name made lowercase.
    seguridad = models.IntegerField(db_column='SEGURIDAD')  # Field name made lowercase.
    actualizado = models.CharField(db_column='ACTUALIZADO', max_length=1)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'configuracion_ref'


class DetalleCompras(models.Model):
    codigo_detalle_compras = models.AutoField(db_column='CODIGO_DETALLE_COMPRAS', primary_key=True)  # Field name made lowercase.
    codigo_compras = models.IntegerField(db_column='CODIGO_COMPRAS')  # Field name made lowercase.
    codigo_producto = models.IntegerField(db_column='CODIGO_PRODUCTO', blank=True, null=True)  # Field name made lowercase.
    cantidad = models.IntegerField(db_column='CANTIDAD')  # Field name made lowercase.
    precio = models.IntegerField(db_column='PRECIO')  # Field name made lowercase.
    igv = models.IntegerField(db_column='IGV')  # Field name made lowercase.
    subtotal = models.IntegerField(db_column='SUBTOTAL')  # Field name made lowercase.
    estado = models.CharField(db_column='ESTADO', max_length=15)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'detalle_compras'


class DetalleReportesz(models.Model):
    codigo_detalle_reportez = models.AutoField(db_column='CODIGO_DETALLE_REPORTEZ', primary_key=True)  # Field name made lowercase.
    codigo_reportez = models.IntegerField(db_column='CODIGO_REPORTEZ')  # Field name made lowercase.
    codigo_producto = models.IntegerField(db_column='CODIGO_PRODUCTO')  # Field name made lowercase.
    cantidad = models.IntegerField(db_column='CANTIDAD')  # Field name made lowercase.
    precio = models.IntegerField(db_column='PRECIO')  # Field name made lowercase.
    igv = models.IntegerField(db_column='IGV')  # Field name made lowercase.
    subtotal = models.IntegerField(db_column='SUBTOTAL')  # Field name made lowercase.
    estado = models.CharField(db_column='ESTADO', max_length=15)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'detalle_reportesz'


class DetalleReporteszRef(models.Model):
    codigo_detalle_reportez = models.AutoField(db_column='CODIGO_DETALLE_REPORTEZ', primary_key=True)  # Field name made lowercase.
    codigo_reportez = models.IntegerField(db_column='CODIGO_REPORTEZ')  # Field name made lowercase.
    codigo_producto = models.IntegerField(db_column='CODIGO_PRODUCTO')  # Field name made lowercase.
    cantidad = models.IntegerField(db_column='CANTIDAD')  # Field name made lowercase.
    precio = models.IntegerField(db_column='PRECIO')  # Field name made lowercase.
    igv = models.IntegerField(db_column='IGV')  # Field name made lowercase.
    subtotal = models.IntegerField(db_column='SUBTOTAL')  # Field name made lowercase.
    estado = models.CharField(db_column='ESTADO', max_length=15)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'detalle_reportesz_ref'


class DetalleSalidas(models.Model):
    codigo_detalle_salidas = models.AutoField(db_column='CODIGO_DETALLE_SALIDAS', primary_key=True)  # Field name made lowercase.
    codigo_salidas = models.IntegerField(db_column='CODIGO_SALIDAS')  # Field name made lowercase.
    codigo_producto = models.IntegerField(db_column='CODIGO_PRODUCTO')  # Field name made lowercase.
    cantidad = models.IntegerField(db_column='CANTIDAD')  # Field name made lowercase.
    precio = models.IntegerField(db_column='PRECIO')  # Field name made lowercase.
    igv = models.IntegerField(db_column='IGV')  # Field name made lowercase.
    subtotal = models.IntegerField(db_column='SUBTOTAL')  # Field name made lowercase.
    estado = models.CharField(db_column='ESTADO', max_length=15)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'detalle_salidas'


class DetalleVentas(models.Model):
    codigo_detalle_ventas = models.AutoField(db_column='CODIGO_DETALLE_VENTAS', primary_key=True)  # Field name made lowercase.
    codigo_ventas = models.IntegerField(db_column='CODIGO_VENTAS')  # Field name made lowercase.
    codigo_producto = models.IntegerField(db_column='CODIGO_PRODUCTO')  # Field name made lowercase.
    cantidad = models.IntegerField(db_column='CANTIDAD')  # Field name made lowercase.
    precio = models.IntegerField(db_column='PRECIO')  # Field name made lowercase.
    igv = models.IntegerField(db_column='IGV')  # Field name made lowercase.
    subtotal = models.IntegerField(db_column='SUBTOTAL')  # Field name made lowercase.
    estado = models.CharField(db_column='ESTADO', max_length=15)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'detalle_ventas'


class Inventario(models.Model):
    codigo_inventario = models.AutoField(db_column='CODIGO_INVENTARIO', primary_key=True)  # Field name made lowercase.
    codigo_producto = models.IntegerField(db_column='CODIGO_PRODUCTO')  # Field name made lowercase.
    fecha_registro = models.DateField(db_column='FECHA_REGISTRO')  # Field name made lowercase.
    cantidad = models.IntegerField(db_column='CANTIDAD')  # Field name made lowercase.
    precio = models.IntegerField(db_column='PRECIO')  # Field name made lowercase.
    estado = models.CharField(db_column='ESTADO', max_length=10)  # Field name made lowercase.
    codigo_bodega = models.IntegerField(db_column='CODIGO_BODEGA')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'inventario'


class Marca(models.Model):
    codigo_marca = models.AutoField(db_column='CODIGO_MARCA', primary_key=True)  # Field name made lowercase.
    descripcion = models.CharField(db_column='DESCRIPCION', max_length=100)  # Field name made lowercase.
    estado = models.CharField(db_column='ESTADO', max_length=10)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'marca'


class Metodopago(models.Model):
    codigo_reportesz = models.IntegerField(db_column='CODIGO_REPORTESZ', blank=True, null=True)  # Field name made lowercase.
    descripcion = models.CharField(db_column='DESCRIPCION', max_length=60, blank=True, null=True)  # Field name made lowercase.
    cantidad = models.IntegerField(db_column='CANTIDAD', blank=True, null=True)  # Field name made lowercase.
    valor = models.IntegerField(db_column='VALOR', blank=True, null=True)  # Field name made lowercase.
    createdat = models.DateTimeField(db_column='createdAt')  # Field name made lowercase.
    updatedat = models.DateTimeField(db_column='updatedAt')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'metodopago'


class Perfil(models.Model):
    codigo_perfil = models.AutoField(db_column='CODIGO_PERFIL', primary_key=True)  # Field name made lowercase.
    descripcion = models.CharField(db_column='DESCRIPCION', max_length=30)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'perfil'


class PerfilRecurso(models.Model):
    codigo_perfil_recurso = models.AutoField(db_column='CODIGO_PERFIL_RECURSO', primary_key=True)  # Field name made lowercase.
    codigo_perfil = models.IntegerField(db_column='CODIGO_PERFIL')  # Field name made lowercase.
    codigo_recurso = models.IntegerField(db_column='CODIGO_RECURSO')  # Field name made lowercase.
    consultar = models.IntegerField(db_column='CONSULTAR')  # Field name made lowercase.
    agregar = models.IntegerField(db_column='AGREGAR')  # Field name made lowercase.
    editar = models.IntegerField(db_column='EDITAR')  # Field name made lowercase.
    eliminar = models.IntegerField(db_column='ELIMINAR')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'perfil_recurso'


class Perfilempresa(models.Model):
    codigo_perfilempresa = models.AutoField(db_column='CODIGO_PERFILEMPRESA', primary_key=True)  # Field name made lowercase.
    nombre = models.CharField(db_column='NOMBRE', max_length=100)  # Field name made lowercase.
    razon_social = models.CharField(db_column='RAZON_SOCIAL', max_length=100, blank=True, null=True)  # Field name made lowercase.
    nit = models.CharField(db_column='NIT', max_length=15, blank=True, null=True)  # Field name made lowercase.
    direccion = models.CharField(db_column='DIRECCION', max_length=50, blank=True, null=True)  # Field name made lowercase.
    regimen = models.CharField(db_column='REGIMEN', max_length=50)  # Field name made lowercase.
    resolucion = models.CharField(db_column='RESOLUCION', max_length=50)  # Field name made lowercase.
    tipo_documento = models.IntegerField(db_column='TIPO_DOCUMENTO')  # Field name made lowercase.
    tipo_maquina = models.CharField(db_column='TIPO_MAQUINA', max_length=3)  # Field name made lowercase.
    prefijo = models.CharField(db_column='PREFIJO', max_length=10, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'perfilempresa'


class Presentacion(models.Model):
    codigo_presentacion = models.AutoField(db_column='CODIGO_PRESENTACION', primary_key=True)  # Field name made lowercase.
    descripcion = models.CharField(db_column='DESCRIPCION', max_length=100)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'presentacion'


class ProdUpd(models.Model):
    codigo_prod_upd = models.AutoField(db_column='CODIGO_PROD_UPD', unique=True)  # Field name made lowercase.
    codigo_producto = models.IntegerField(db_column='CODIGO_PRODUCTO')  # Field name made lowercase.
    caja1 = models.CharField(db_column='CAJA1', max_length=3)  # Field name made lowercase.
    caja2 = models.CharField(db_column='CAJA2', max_length=3)  # Field name made lowercase.
    caja3 = models.CharField(db_column='CAJA3', max_length=3)  # Field name made lowercase.
    caja4 = models.CharField(db_column='CAJA4', max_length=3)  # Field name made lowercase.
    caja5 = models.CharField(db_column='CAJA5', max_length=3)  # Field name made lowercase.
    caja6 = models.CharField(db_column='CAJA6', max_length=3)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'prod_upd'


class Producto(models.Model):
    codigo_producto = models.AutoField(db_column='CODIGO_PRODUCTO', primary_key=True)  # Field name made lowercase.
    codigo_categoria = models.IntegerField(db_column='CODIGO_CATEGORIA')  # Field name made lowercase.
    codigo_presentacion = models.IntegerField(db_column='CODIGO_PRESENTACION')  # Field name made lowercase.
    codigo_marca = models.IntegerField(db_column='CODIGO_MARCA')  # Field name made lowercase.
    codigo_barra = models.CharField(db_column='CODIGO_BARRA', max_length=15)  # Field name made lowercase.
    referencia = models.CharField(db_column='REFERENCIA', max_length=30)  # Field name made lowercase.
    descripcion = models.CharField(db_column='DESCRIPCION', max_length=100)  # Field name made lowercase.
    unidad_medida = models.CharField(db_column='UNIDAD_MEDIDA', max_length=50)  # Field name made lowercase.
    stock_minimo = models.IntegerField(db_column='STOCK_MINIMO')  # Field name made lowercase.
    stock = models.IntegerField(db_column='STOCK')  # Field name made lowercase.
    precio_compra = models.IntegerField(db_column='PRECIO_COMPRA')  # Field name made lowercase.
    precio = models.IntegerField(db_column='PRECIO')  # Field name made lowercase.
    fecha_ingreso = models.DateTimeField(db_column='FECHA_INGRESO')  # Field name made lowercase.
    referencia_02 = models.CharField(db_column='REFERENCIA_02', max_length=50)  # Field name made lowercase.
    precio_mayorista = models.CharField(db_column='PRECIO_MAYORISTA', max_length=50)  # Field name made lowercase.
    estado = models.CharField(db_column='ESTADO', max_length=10)  # Field name made lowercase.
    actualizado = models.CharField(db_column='ACTUALIZADO', max_length=6)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'producto'


class Proveedor(models.Model):
    codigo_proveedor = models.AutoField(db_column='CODIGO_PROVEEDOR', primary_key=True)  # Field name made lowercase.
    tipo_contribuyente = models.CharField(db_column='TIPO_CONTRIBUYENTE', max_length=30)  # Field name made lowercase.
    ruc = models.CharField(db_column='RUC', max_length=20)  # Field name made lowercase.
    razon_social = models.CharField(db_column='RAZON_SOCIAL', max_length=100)  # Field name made lowercase.
    representante = models.CharField(db_column='REPRESENTANTE', max_length=100)  # Field name made lowercase.
    celular = models.CharField(db_column='CELULAR', max_length=30)  # Field name made lowercase.
    direccion = models.CharField(db_column='DIRECCION', max_length=100)  # Field name made lowercase.
    telefono = models.CharField(db_column='TELEFONO', max_length=30)  # Field name made lowercase.
    email = models.CharField(db_column='EMAIL', max_length=100)  # Field name made lowercase.
    ciudad = models.CharField(db_column='CIUDAD', max_length=100)  # Field name made lowercase.
    estado = models.CharField(db_column='ESTADO', max_length=10)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'proveedor'


class Proyeccion(models.Model):
    codigo_proyeccion = models.AutoField(db_column='CODIGO_PROYECCION', primary_key=True)  # Field name made lowercase.
    fecha = models.DateTimeField(db_column='FECHA')  # Field name made lowercase.
    codigo_caja = models.IntegerField(db_column='CODIGO_CAJA')  # Field name made lowercase.
    sucursal = models.CharField(db_column='SUCURSAL', max_length=100)  # Field name made lowercase.
    valor = models.IntegerField(db_column='VALOR')  # Field name made lowercase.
    estado = models.CharField(db_column='ESTADO', max_length=5, db_collation='utf8_general_ci')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'proyeccion'


class Recursos(models.Model):
    codigo_recurso = models.AutoField(db_column='CODIGO_RECURSO', primary_key=True)  # Field name made lowercase.
    nombre = models.CharField(db_column='NOMBRE', max_length=100)  # Field name made lowercase.
    fecha_registro = models.DateTimeField(db_column='FECHA_REGISTRO')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'recursos'


class ReporteszRef(models.Model):
    codigo_reportesz = models.AutoField(db_column='CODIGO_REPORTESZ', primary_key=True)  # Field name made lowercase.
    serial = models.CharField(db_column='SERIAL', max_length=30)  # Field name made lowercase.
    cajero = models.IntegerField(db_column='CAJERO')  # Field name made lowercase.
    caja = models.CharField(db_column='CAJA', max_length=10)  # Field name made lowercase.
    numeracion = models.CharField(db_column='NUMERACION', max_length=7)  # Field name made lowercase.
    tipo_z = models.CharField(db_column='TIPO_Z', max_length=5)  # Field name made lowercase.
    numero_z = models.IntegerField(db_column='NUMERO_Z')  # Field name made lowercase.
    fecha_reporte = models.DateTimeField(db_column='FECHA_REPORTE')  # Field name made lowercase.
    fecha_open = models.DateTimeField(db_column='FECHA_OPEN')  # Field name made lowercase.
    fecha_close = models.DateTimeField(db_column='FECHA_CLOSE')  # Field name made lowercase.
    total_tarjetas = models.IntegerField(db_column='TOTAL_TARJETAS', blank=True, null=True)  # Field name made lowercase.
    total_efectivo = models.IntegerField(db_column='TOTAL_EFECTIVO', blank=True, null=True)  # Field name made lowercase.
    cantidad_tarjetas = models.IntegerField(db_column='CANTIDAD_TARJETAS', blank=True, null=True)  # Field name made lowercase.
    cantidad_efectivo = models.IntegerField(db_column='CANTIDAD_EFECTIVO', blank=True, null=True)  # Field name made lowercase.
    base = models.IntegerField(db_column='BASE', blank=True, null=True)  # Field name made lowercase.
    iva = models.IntegerField(db_column='IVA', blank=True, null=True)  # Field name made lowercase.
    gross_sales = models.IntegerField(db_column='GROSS_SALES', blank=True, null=True)  # Field name made lowercase.
    net_sales = models.IntegerField(db_column='NET_SALES', blank=True, null=True)  # Field name made lowercase.
    cantidad = models.IntegerField(db_column='CANTIDAD', blank=True, null=True)  # Field name made lowercase.
    valor_cantidad = models.IntegerField(db_column='VALOR_CANTIDAD', blank=True, null=True)  # Field name made lowercase.
    cantidad_2 = models.IntegerField(db_column='CANTIDAD_2', blank=True, null=True)  # Field name made lowercase.
    valor_cantidad_2 = models.IntegerField(db_column='VALOR_CANTIDAD_2', blank=True, null=True)  # Field name made lowercase.
    porcentaje = models.IntegerField(db_column='PORCENTAJE', blank=True, null=True)  # Field name made lowercase.
    total_due = models.IntegerField(db_column='TOTAL_DUE')  # Field name made lowercase.
    cash_due = models.IntegerField(db_column='CASH_DUE')  # Field name made lowercase.
    cons_inicial = models.IntegerField(db_column='CONS_INICIAL')  # Field name made lowercase.
    cons_final = models.IntegerField(db_column='CONS_FINAL')  # Field name made lowercase.
    estado = models.CharField(db_column='ESTADO', max_length=15)  # Field name made lowercase.
    total_refund = models.IntegerField(db_column='TOTAL_REFUND')  # Field name made lowercase.
    total_remove = models.IntegerField(db_column='TOTAL_REMOVE')  # Field name made lowercase.
    total_void = models.IntegerField(db_column='TOTAL_VOID')  # Field name made lowercase.
    cantidad_refund = models.IntegerField(db_column='CANTIDAD_REFUND')  # Field name made lowercase.
    cantidad_remove = models.IntegerField(db_column='CANTIDAD_REMOVE')  # Field name made lowercase.
    cantidad_void = models.IntegerField(db_column='CANTIDAD_VOID')  # Field name made lowercase.
    descuento = models.IntegerField(db_column='DESCUENTO')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'reportesz_ref'


class Salidas(models.Model):
    codigo_salidas = models.AutoField(db_column='CODIGO_SALIDAS', primary_key=True)  # Field name made lowercase.
    codigo_cliente = models.IntegerField(db_column='CODIGO_CLIENTE')  # Field name made lowercase.
    codigo_usuario = models.IntegerField(db_column='CODIGO_USUARIO')  # Field name made lowercase.
    tipo_documento = models.CharField(db_column='TIPO_DOCUMENTO', max_length=50)  # Field name made lowercase.
    serie = models.CharField(db_column='SERIE', max_length=4)  # Field name made lowercase.
    numeracion = models.CharField(db_column='NUMERACION', max_length=7)  # Field name made lowercase.
    tipo_pago = models.CharField(db_column='TIPO_PAGO', max_length=50)  # Field name made lowercase.
    fecha_salida = models.DateTimeField(db_column='FECHA_SALIDA')  # Field name made lowercase.
    monto_recibido = models.IntegerField(db_column='MONTO_RECIBIDO', blank=True, null=True)  # Field name made lowercase.
    total = models.IntegerField(db_column='TOTAL')  # Field name made lowercase.
    codigo_bodega_origen = models.IntegerField(db_column='CODIGO_BODEGA_ORIGEN')  # Field name made lowercase.
    codigo_bodega_destino = models.IntegerField(db_column='CODIGO_BODEGA_DESTINO')  # Field name made lowercase.
    descripcion = models.TextField(db_column='DESCRIPCION')  # Field name made lowercase.
    estado = models.CharField(db_column='ESTADO', max_length=15)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'salidas'


class StockBodegas(models.Model):
    codigo_stock_bodega = models.AutoField(db_column='CODIGO_STOCK_BODEGA', primary_key=True)  # Field name made lowercase.
    codigo_producto = models.IntegerField(db_column='CODIGO_PRODUCTO')  # Field name made lowercase.
    codigo_bodega = models.IntegerField(db_column='CODIGO_BODEGA')  # Field name made lowercase.
    stock = models.IntegerField(db_column='STOCK')  # Field name made lowercase.
    estado = models.CharField(db_column='ESTADO', max_length=10)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'stock_bodegas'


class TipoDocumentos(models.Model):
    codigo_documento = models.AutoField(db_column='CODIGO_DOCUMENTO', primary_key=True)  # Field name made lowercase.
    nombre = models.CharField(db_column='NOMBRE', max_length=50)  # Field name made lowercase.
    numeracion = models.CharField(db_column='NUMERACION', max_length=5)  # Field name made lowercase.
    nro_inicial = models.CharField(db_column='NRO_INICIAL', max_length=7)  # Field name made lowercase.
    nro_final = models.CharField(db_column='NRO_FINAL', max_length=7)  # Field name made lowercase.
    fecha_registro = models.DateTimeField(db_column='FECHA_REGISTRO')  # Field name made lowercase.
    estado = models.CharField(db_column='ESTADO', max_length=10)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'tipo_documentos'


class Usuario(models.Model):
    codigo_usuario = models.AutoField(db_column='CODIGO_USUARIO', primary_key=True)  # Field name made lowercase.
    nombres = models.CharField(db_column='NOMBRES', max_length=100)  # Field name made lowercase.
    usuario = models.CharField(db_column='USUARIO', max_length=100)  # Field name made lowercase.
    email = models.CharField(db_column='EMAIL', max_length=100)  # Field name made lowercase.
    clave = models.CharField(db_column='CLAVE', max_length=100)  # Field name made lowercase.
    fecha_registro = models.DateTimeField(db_column='FECHA_REGISTRO')  # Field name made lowercase.
    estado = models.CharField(db_column='ESTADO', max_length=10)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'usuario'


class UsuarioPerfiles(models.Model):
    codigo_usuario_perfiles = models.AutoField(db_column='CODIGO_USUARIO_PERFILES', primary_key=True)  # Field name made lowercase.
    codigo_usuario = models.IntegerField(db_column='CODIGO_USUARIO')  # Field name made lowercase.
    codigo_perfil = models.IntegerField(db_column='CODIGO_PERFIL')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'usuario_perfiles'


class UsuarioRef(models.Model):
    codigo_usuario = models.AutoField(db_column='CODIGO_USUARIO', primary_key=True)  # Field name made lowercase.
    nombres = models.CharField(db_column='NOMBRES', max_length=100)  # Field name made lowercase.
    usuario = models.CharField(db_column='USUARIO', max_length=100)  # Field name made lowercase.
    estado = models.CharField(db_column='ESTADO', max_length=10)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'usuario_ref'


class VCompras(models.Model):
    id = models.IntegerField()
    fecha = models.DateField()
    fecha_b = models.DateField(db_column='FECHA_B')  # Field name made lowercase.
    id_tip_doc = models.CharField(max_length=30, db_collation='utf8_general_ci')
    tipo_documento = models.CharField(max_length=50, db_collation='utf8_general_ci')
    nro_comprobante = models.CharField(max_length=7, db_collation='utf8_general_ci')
    proveedor = models.CharField(max_length=100, db_collation='utf8_general_ci')
    ruc = models.CharField(max_length=20, db_collation='utf8_general_ci')
    tipo_pago = models.CharField(max_length=20, db_collation='utf8_general_ci')
    total = models.IntegerField()
    estado = models.CharField(max_length=15, db_collation='utf8_general_ci')

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'v_compras'


class VComprasdepartamentos(models.Model):
    fecha_b = models.DateField(db_column='FECHA_B')  # Field name made lowercase.
    id = models.IntegerField(blank=True, null=True)
    departamento = models.CharField(max_length=100, db_collation='utf8_general_ci', blank=True, null=True)
    cantidad = models.IntegerField(blank=True, null=True)
    total = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'v_comprasdepartamentos'


class Ventas(models.Model):
    codigo_ventas = models.AutoField(db_column='CODIGO_VENTAS', primary_key=True)  # Field name made lowercase.
    codigo_cliente = models.IntegerField(db_column='CODIGO_CLIENTE')  # Field name made lowercase.
    codigo_usuario = models.IntegerField(db_column='CODIGO_USUARIO')  # Field name made lowercase.
    tipo_documento = models.CharField(db_column='TIPO_DOCUMENTO', max_length=50)  # Field name made lowercase.
    serie = models.IntegerField(db_column='SERIE')  # Field name made lowercase.
    numeracion = models.IntegerField(db_column='NUMERACION')  # Field name made lowercase.
    tipo_pago = models.CharField(db_column='TIPO_PAGO', max_length=50)  # Field name made lowercase.
    fecha_venta = models.DateTimeField(db_column='FECHA_VENTA', blank=True, null=True)  # Field name made lowercase.
    monto_recibido = models.IntegerField(db_column='MONTO_RECIBIDO', blank=True, null=True)  # Field name made lowercase.
    total = models.IntegerField(db_column='TOTAL')  # Field name made lowercase.
    total_tarjeta = models.IntegerField(db_column='TOTAL_TARJETA')  # Field name made lowercase.
    codigo_bodega = models.IntegerField(db_column='CODIGO_BODEGA')  # Field name made lowercase.
    descripcion = models.TextField(db_column='DESCRIPCION')  # Field name made lowercase.
    estado = models.CharField(db_column='ESTADO', max_length=15)  # Field name made lowercase.
    descuento = models.IntegerField(db_column='DESCUENTO')  # Field name made lowercase.
    codigo_vendedor = models.IntegerField(db_column='CODIGO_VENDEDOR')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ventas'
