from django.urls import path, register_converter
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)
from apiPrueba.views import userCreateView, ventasView
from apiPrueba.views import detalleView



urlpatterns = [
path('login/', TokenObtainPairView.as_view()),
path('refresh/', TokenRefreshView.as_view()),
path('ventas/<int:pk>/',ventasView.VentasDetailView.as_view()), # check info for an specific user based on id(pk)
path('ventas/detalle/<int:pk>/',detalleView.DetalleView.as_view()), # check info for an specific user based on id(
path('user/create',userCreateView.UserCreateView.as_view()),    
]